package ictgradschool.industry.lab05.ex02;

/**
 * Represents a Bird.
 *
 * TODO Correctly implement these methods, as instructed in the lab handout.
 */
public class Bird implements IAnimal {

    @Override
    public String sayHello() {
        String birdSays = "tweet tweet";
        return birdSays;
    }

    @Override
    public boolean isMammal() {

        return false;
    }

    @Override
    public String myName() {
        String name = "Tweety the bird";
        return name;
    }

    @Override
    public int legCount() {
        int numOfLegs = 2;
        return numOfLegs;
    }
}
