package ictgradschool.industry.lab05.ex02;

/**
 * Represents a dog.
 *
 * TODO Make this class implement the IAnimal interface, then implement all its methods.
 */
public class Dog implements IAnimal {

    @Override
    public String sayHello() {
        String dogSays = "woof woof";
        return dogSays;
    }

    @Override
    public boolean isMammal() {

        return true;
    }

    @Override
    public String myName() {
        String name = "Bruno the dog";
        return name;
    }

    @Override
    public int legCount() {
        int numOfLegs = 4;
        return numOfLegs;
    }
}
