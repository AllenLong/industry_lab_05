package ictgradschool.industry.lab05.ex02;

/**
 * Represents a horse.
 *
 * TODO Make this implement IAnimal and IFamous, and provide appropriate implementations of those methods.
 */
public class Horse implements IAnimal, IFamous {

    @Override
    public String sayHello() {
        String horseSays = "neigh";
        return horseSays;
    }

    @Override
    public boolean isMammal() {

        return true;
    }

    @Override
    public String myName() {
        String name = "Mr.Ed the horse";
        return name;
    }

    @Override
    public int legCount() {
        int numOfLegs = 4;
        return numOfLegs;
    }

    public String famous(){
        String famousName= "PharLap";
        return famousName;
    }
}
