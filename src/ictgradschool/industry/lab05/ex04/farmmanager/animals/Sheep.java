package ictgradschool.industry.lab05.ex04.farmmanager.animals;


public class Sheep implements Animal {


    private static final String name = "Sheep";

    private int value;

    public Sheep() {
        value = 800;
    }

    public void feed() {
        if (value < 1000) {
            value += 60;
        }
    }

    public int costToFeed() {
        return 30;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String toString() {
        return name + " - $" + value;
    }
}